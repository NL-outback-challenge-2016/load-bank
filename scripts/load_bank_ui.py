#!/usr/bin/env python

import os
import threading
from threading import Thread

import rospy
from load_bank.msg import Load
from std_msgs.msg import UInt8

import curses
from curses import wrapper
from curses import ascii


class Console:
    TITLE_ROW = 1
    MASTER_ROW = 3
    PINS_ROW = 4
    CURRENT_ROW = 5
    VOLTAGE_ROW = 6
    COMMAND_ROW = 7

    def __init__(self):
        self.pins_count = 0
        self.current_amps = 0
        self.voltage_volts = 0
        self.command_power = 0
        self.promptMsg = ''
        self.publisher = None


    def paint(self):
        if self.screen == None:
            return

        self.screen.addstr(0, 0, ' ' * curses.COLS, curses.A_REVERSE)
        self.screen.addstr(self.TITLE_ROW, 0, ' ' * curses.COLS, curses.A_REVERSE)
        self.screen.addstr( self.TITLE_ROW, 0, 'Load Bank Control', curses.A_REVERSE)

        self.screen.addstr(self.MASTER_ROW, 0,    'ROS Master : ')
        self.screen.addstr(str(os.environ['ROS_MASTER_URI']))

        self.screen.addstr(self.PINS_ROW, 0,      'Pins count  : ')
        self.screen.addstr("{0:2d}".format(self.pins_count))

        self.screen.addstr(self.CURRENT_ROW, 0,   'Current (A) : ')
        self.screen.addstr(str(self.current_amps))

        self.screen.addstr(self.VOLTAGE_ROW, 0,   'Voltage (V) : ')
        self.screen.addstr(str(self.voltage_volts))

        self.screen.addstr(self.COMMAND_ROW, 0,   'Power SP (%): ')
        self.screen.addstr("{0:3d}".format(self.command_power))

        self.screen.addstr(curses.LINES - 2, 0, ' ' * curses.COLS, curses.A_REVERSE)
        self.screen.addstr(curses.LINES - 2, 0,   
                           'Power: [0-9] or UP or DOWN keys, Stop: SPACE, Quit: q', 
                           curses.A_REVERSE)

        self.screen.addstr(curses.LINES - 1, 0,  ' ' * 20)
        self.screen.addstr(curses.LINES - 1, 0, '> %s' % self.promptMsg)
        if '' != self.promptMsg:
            self.promptMsg = '       '

        self.screen.refresh()

    
    def set_power(self, power):
        self.command_power = int(power)


    def prompt(self, msg):
        self.promptMsg = msg


    def run(self, screen):
        self.screen = screen
        self.screen.nodelay(1)
        rate = rospy.Rate(15)
        while not rospy.is_shutdown():

            self.paint()
            c = screen.getch()
            
            if c == -1:
                rate.sleep()

            if curses.ascii.isalnum(c):
                self.prompt(chr(c))
            else:
                self.prompt('')

            if c == ord('q'):
                break
            if c == curses.ascii.ESC:
                break

            if c == curses.ascii.SP:
                self.set_power(0)
            if c == ord('0'):
                self.set_power(0)
            if c == ord('1'):
                self.set_power(10)
            if c == ord('2'):
                self.set_power(20)
            if c == ord('3'):
                self.set_power(30)
            if c == ord('4'):
                self.set_power(40)
            if c == ord('5'):
                self.set_power(50)
            if c == ord('6'):
                self.set_power(60)
            if c == ord('7'):
                self.set_power(70)
            if c == ord('8'):
                self.set_power(80)
            if c == ord('9'):
                self.set_power(90)

            if c == curses.KEY_UP:
                self.command_power = self.command_power + 1
                self.command_power = min(100, self.command_power)
                self.set_power(self.command_power)
            if c == curses.KEY_DOWN:
                self.command_power = self.command_power - 1
                self.command_power = max(0, self.command_power)
                self.set_power(self.command_power)


console = Console()


def update_load_percent(load_msg):
    console.pins_count = load_msg.pins_count
    console.current_amps = load_msg.current_amps
    console.voltage_volts = load_msg.voltage_volts


def push_set_points():
    rate = rospy.Rate(1)
    while not rospy.is_shutdown():
        console.publisher.publish(UInt8(console.command_power))
        rate.sleep()


def main(screen):

    rospy.init_node('load_bank_ui')

    # Display telemetry from load bank
    rospy.Subscriber('/load_bank/load_percent', Load, update_load_percent)

    # Send set-point commands at a specified frequency
    console.publisher = rospy.Publisher('/load_bank/command_load_percent', UInt8, queue_size=10)
    t = threading.Thread(target=push_set_points)
    t.setDaemon(True)
    t.start()
    
    # Bring up the "UI"
    console.run(screen)


wrapper(main)
